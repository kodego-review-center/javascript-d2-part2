console.log ("Connected to HTML"); 

/* Assignment Operator (=) */
let a = 14;
a = 18;

let b = a;
console.log(b);

//Arithmetic Operators
//Examples:

//addition (+)
console.log (20+30);

console.log ("20"+"30");
console.log (`20`+ `30`);
console.log ("Twenty" + "Thirty");
console.log (`Twenty` + `Thirty`);
console.log ('20'+30);

//difference (-)
console.log(20 - 10);		
console.log("20" - "10"); //coercion 
let val1 = 30;
let val2 = '50';
let result = val1 - val2;
console.log(result);
console.log("Twenty" - "Ten"); //NaN

//division (/)
console.log(50 / 5);		//10
console.log("50"/"5"); //coercion 
console.log ("Fifty"/"Five");

//Increment (++) && Decrement (--)
let c = 30;

console.log( ++c );		//31
console.log(c);			//31

console.log( --c );		//30
console.log(c);			//30

console.log( c++ );		//30
console.log(c);			//31 - current value of C

console.log( c-- );		//31
console.log(c);			//30

let h = 4;

console.log( (52 - 29) * h );

//Compound Assignment Operators

